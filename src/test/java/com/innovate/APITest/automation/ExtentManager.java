package com.innovate.APITest.automation;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
// This Java class configures Extent report. This class automatically creates Extent HTML in the Screenshots folder and shows JDK version, Environment, Appium Version and Developer details in the Dash board version of the report.
public class ExtentManager {
    public static Properties prop = new Properties();

    public static ExtentReports extent;
    public static String filePath = System.getProperty("user.dir") + "/Report/InnovateAPIautomation.html";

    public ExtentManager() throws IOException {
        prop.load(new FileInputStream("config.properties"));
    }

    public synchronized static ExtentReports getExtent() {
        if (extent == null) {
            extent = new ExtentReports();
            extent.attachReporter(getHtmlReporter());
            extent.setSystemInfo("JDK Version", "1.8.0");
            extent.setSystemInfo("Environment", "Android");
            extent.setSystemInfo("API", "LondonWeather");
            extent.setSystemInfo("TestDeveloper", "Rakesh kumar kandanuru");
            
        }

        return extent;
    }

    private static ExtentHtmlReporter getHtmlReporter() {
        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(filePath);
        htmlReporter.loadXMLConfig("./config.xml");
        htmlReporter.config().setChartVisibilityOnOpen(true);
        htmlReporter.config().setDocumentTitle("MobileAppAutomationTest");
        htmlReporter.config().setReportName("TREK APP Android Automation Report");
        htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
        htmlReporter.config().setTheme(Theme.STANDARD);

        return htmlReporter;
    }

}