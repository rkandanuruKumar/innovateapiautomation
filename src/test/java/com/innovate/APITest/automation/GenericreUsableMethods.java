package com.innovate.APITest.automation;


import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.io.*;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
//import java.time.Duration;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Properties;
import java.util.UUID;

public class GenericreUsableMethods {
	
	ExtentReports extent;

	public String getintExpectedVal(String nameToSearch) throws IOException {
		String fileLocation = System.getProperty("user.dir") + "/AutomationInnovateAPI.xlsx";
		XSSFWorkbook wb = new XSSFWorkbook(fileLocation);
		for (int sheetIndex = 0; sheetIndex < wb.getNumberOfSheets(); sheetIndex++) {
			XSSFSheet sheet = wb.getSheetAt(sheetIndex);
			for (int rowIndex = 0; rowIndex < sheet.getLastRowNum(); rowIndex++) {
				XSSFRow row = sheet.getRow(rowIndex);

				if (row != null && row.getCell(6).getStringCellValue().equals(nameToSearch)) {
					final DataFormatter df = new DataFormatter();
					final Cell cell = row.getCell(7);
					String valueAsString = df.formatCellValue(cell);
					return valueAsString;
				}
			}
		}
		return "";
	}

	
	
	public String getStringExpectedVal(String nameToSearch) throws IOException {
		String fileLocation = System.getProperty("user.dir") + "/AutomationInnovateAPI.xlsx";
		XSSFWorkbook wb = new XSSFWorkbook(fileLocation);
		for (int sheetIndex = 0; sheetIndex < wb.getNumberOfSheets(); sheetIndex++) {
			XSSFSheet sheet = wb.getSheetAt(sheetIndex);
			for (int rowIndex = 0; rowIndex < sheet.getLastRowNum(); rowIndex++) {
				XSSFRow row = sheet.getRow(rowIndex);
				String cell = sheet.getActiveCell();
				if (row != null && row.getCell(6).getStringCellValue().equals(nameToSearch)) {

					return row.getCell(7).getStringCellValue();

				}
			}
		}
		return "";
	}

}
