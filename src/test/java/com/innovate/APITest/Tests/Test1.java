package com.innovate.APITest.Tests;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.Assert;
import org.testng.AssertJUnit;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.innovate.APITest.automation.*;

import com.relevantcodes.extentreports.LogStatus;

import io.restassured.RestAssured;
import io.restassured.http.Method;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterSuite;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.NoAlertPresentException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.testng.Assert;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;
import java.util.Map;

// Java class is a Test class. This shows the collection of test suite of the API verification.
public class Test1 extends BaseTestAPI {

	private ExtentReports extent;
	private ExtentTest test;

	GenericreUsableMethods genMethods = new GenericreUsableMethods();

	@BeforeSuite
	public void extrepInit() throws MalformedURLException {

		extent = ExtentManager.getExtent();

	}

	@BeforeMethod
	public void setUp() throws MalformedURLException {

		try {

			RestAssured.baseURI = "https://samples.openweathermap.org/data/2.5";
			httpRequest = RestAssured.given();
			response = httpRequest.request(Method.GET, "/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22");
			extent = ExtentManager.getExtent();

		} catch (Exception e) {

			System.out.println("Exception------->" + e.toString());
		}
	}

	
	@Test(priority = 1)
	public void TC001_APIResponse_Validation_NotNull()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {

			test = extent.createTest("Weathermap Response Verification- Location London",
					"Test Case to verify if URI is valid API which can yeild response");
			String responseBody = response.getBody().asString();
			Assert.assertNotNull(responseBody);
			test.pass("API Response is Not Null. Verified API response in Run Time:"+"  " + responseBody);
			
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("Coord Lon") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 2)
	public void TC002_APIResponse_Validation_CoordLon()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			test = extent.createTest("API Response - Coord Section - VerificationResults",
					"Test Case to verify if API Response of Coord section has valid Expected results");
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getintExpectedVal("Coord Lon")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "Coord Lon value: "+ genMethods.getintExpectedVal("Coord Lon")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("Coord Lon") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}

	@Test(priority = 3)
	public void TC003_APIResponse_Validation_CoordLat()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getintExpectedVal("Coord Lat")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "Coord Lat value: "+ genMethods.getintExpectedVal("Coord Lat")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("Coord Lat") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 4)
	public void TC004_APIResponse_Validation_Weatherid()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			test = extent.createTest("API Response - Weather Section - VerificationResults",
					"Test Case to verify if API Response of Weather Section has valid Expected results");
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getintExpectedVal("Weather id")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "Weather id value: "+ genMethods.getintExpectedVal("Weather id")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("Weather id") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 5)
	public void TC005_APIResponse_Validation_Weathermain()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getStringExpectedVal("Weather main")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "Weather main value: "+ genMethods.getStringExpectedVal("Weather main")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("Weather main") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 6)
	public void TC006_APIResponse_Validation_Weatherdescription()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getStringExpectedVal("Weather description")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "Weather description value: "+ genMethods.getStringExpectedVal("Weather description")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("Weather description") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}

	@Test(priority = 7)
	public void TC007_APIResponse_Validation_Weathericon()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getStringExpectedVal("Weather icon")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "Weather icon value: "+ genMethods.getStringExpectedVal("Weather icon")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("Weather icon") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 8)
	public void TC008_APIResponse_Validation_Weatherbase()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getStringExpectedVal("Weather base")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "Weather base value: "+ genMethods.getStringExpectedVal("Weather base")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("Weather base") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 9)
	public void TC009_APIResponse_Validation_maintemp()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			test = extent.createTest("API Response - main Section - VerificationResults",
					"Test Case to verify if API Response of main Section has valid Expected results");
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getintExpectedVal("main temp")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "main temp value: "+ genMethods.getintExpectedVal("main temp")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("main temp") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 10)
	public void TC010_APIResponse_Validation_mainpressure()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getintExpectedVal("main pressure")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "main pressure value: "+ genMethods.getintExpectedVal("main pressure")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("main pressure") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 11)
	public void TC011_APIResponse_Validation_mainhumidity()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getintExpectedVal("main humidity")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "main humidity value: "+ genMethods.getintExpectedVal("main humidity")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("main humidity") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 12)
	public void TC012_APIResponse_Validation_maintemp_min()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getintExpectedVal("main temp_min")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "main temp_min value: "+ genMethods.getStringExpectedVal("main temp_min")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getStringExpectedVal("main temp_min") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 13)
	public void TC013_APIResponse_Validation_maintemp_max()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getintExpectedVal("main temp_max").toString()));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "main temp_max value: "+ genMethods.getintExpectedVal("main temp_max")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("main temp_max") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 14)
	public void TC014_APIResponse_Validation_mainvisibility()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getintExpectedVal("main visibility")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "main visibility value: "+ genMethods.getintExpectedVal("main visibility")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("main visibility") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 15)
	public void TC015_APIResponse_Validation_windspeed()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			test = extent.createTest("API Response - Wind Section - VerificationResults",
					"Test Case to verify if API Response of Wind Section has valid Expected results");
			
			String responseBody = response.getBody().asString();
			
			Assert.assertTrue(responseBody.contains(genMethods.getintExpectedVal("wind speed")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "wind speed value: "+ genMethods.getintExpectedVal("wind speed")+ " is Found in API.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getStringExpectedVal("wind speed") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 16)
	public void TC016_APIResponse_Validation_winddeg()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getintExpectedVal("wind deg")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "wind deg value: "+ genMethods.getintExpectedVal("wind deg")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("wind deg") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 17)
	public void TC017_APIResponse_Validation_cloudsall()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			test = extent.createTest("API Response - Clouds Section - VerificationResults",
					"Test Case to verify if API Response of Clouds Section has valid Expected results");
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getintExpectedVal("clouds all")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "clouds all value: "+ genMethods.getintExpectedVal("clouds all")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("clouds all") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 18)
	public void TC018_APIResponse_Validation_cloudsdt()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getintExpectedVal("clouds dt")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "clouds dt value: "+ genMethods.getintExpectedVal("clouds dt")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("clouds dt") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 19)
	public void TC019_APIResponse_Validation_systype()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			test = extent.createTest("API Response - Sys Section - VerificationResults",
					"Test Case to verify if API Response of Sys Section has valid Expected results");
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getintExpectedVal("sys type")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "sys type value: "+ genMethods.getintExpectedVal("sys type")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("sys type") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 20)
	public void TC020_APIResponse_Validation_sysid()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getintExpectedVal("sys id")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "sys id value: "+ genMethods.getintExpectedVal("sys id")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("sys id") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 21)
	public void TC021_APIResponse_Validation_sysmessage()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getintExpectedVal("sys message")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "sys message value: "+ genMethods.getintExpectedVal("sys message")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("sys message") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 22)
	public void TC022_APIResponse_Validation_syscountry()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getStringExpectedVal("sys country")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "sys country value: "+ genMethods.getStringExpectedVal("sys country")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("sys country") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 23)
	public void TC023_APIResponse_Validation_syssunrise()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getintExpectedVal("sys sunrise")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "sys sunrise value: "+ genMethods.getintExpectedVal("sys sunrise")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("sys sunrise") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 24)
	public void TC024_APIResponse_Validation_syssunset()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getintExpectedVal("sys sunset")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "sys sunset value: "+ genMethods.getintExpectedVal("sys sunset")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("sys sunset") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 25)
	public void TC025_APIResponse_Validation_sysid()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getintExpectedVal("sys id")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "sys id value: "+ genMethods.getintExpectedVal("sys id")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("sys id") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 26)
	public void TC026_APIResponse_Validation_sysname()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getStringExpectedVal("sys name")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "sys name value: "+ genMethods.getStringExpectedVal("sys name")+ " is Found in API Response.");
		} catch (Exception e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("sys name") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@Test(priority = 27)
	public void TC027_APIResponse_Validation_syscod()
			throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

		try {
			
			String responseBody = response.getBody().asString();
			Assert.assertTrue(responseBody.contains(genMethods.getintExpectedVal("sys cod")));
			test.pass("Test Pass : Expected Result is equal to Actual Result, " 
					+ "sys cod value: "+ genMethods.getintExpectedVal("sys cod")+ " is Found in API Response.");
		} catch (AssertionError e) {
			test.fail("Test fail: Expected result is not equal to Actual result :" + genMethods.getintExpectedVal("sys cod") +"is found in API Response");
			Assert.assertTrue(false);

		}
	}
	
	@AfterSuite
	public void afterClass() {

		extent.flush();

	}
}
